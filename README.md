# Big Data Storage and Processing


## Legacy Approaches
    • Traditional Computing Architecture & Data Storage
    • Relational DBMS(SQL) & Data Silos
    • Old Data (SQL) vs. Big Data
## Distributed Systems and Data Management
    • Architectures
    • Methodologies
    • Scaling
## Big Data Storage
    • Physical Storage
    • Data Processing ETL/ELT
    • Data Tiering
    • File Formats, Compression and Security
    • Disaster Recovery
## No-SQL
    • Key-value (e.g., Couchbase, Redis)
    • Document (e.g., MongoDB, CouchDB)
    • Columnar (e.g., Big Table, Cassandra)
    • Graph (e.g., Neo4j)
    • Spatial (e.g., OGC-compliant)
## Big Data Platforms
    • Apache Hadoop and HDFS
    • MapReduce
    • YARN (resource management)
    • Apache Spark
## Big Data Programming
    • Apache Hive (SQL-like queries)
    • Apache Pig (high-level scripts that run on Apache Hadoop)
    • Apache Mahout (machine learning algorithms on Apache Hadoop)
    • Spark Mllib (scalable and easy machine learning library on Apache Spark)
## Streaming Big Data
    • Spark Streaming
    • Kafka
## Graph Big Data
    • Apache Giraph (Graph processing on Graph Big Data)
