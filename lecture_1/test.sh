#! /bin/bash

# array of no's from 0 to 10
x=( $(seq 0 10 ) )

# display first 10 no's forward
echo ${x[@]}

# display first 10 no's backward
printf '%s\n' "${x[@]}" | tac | tr '\n' ' ';echo

# display even no's between 0 & 10
for i in ${x[@]};
do
        if [ `expr $i % 2` == 0 ];
        then 
                echo $i | tr '\n' ' '
        fi 
done;echo
